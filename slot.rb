require_relative 'vechile'

class Slot
  attr_accessor :id, :vechile

  def initialize(id)
    @id = id.to_i
  end

  def park(registration_number, vechile_colour)
    if self.vechile
      puts "Error"
    else
      self.vechile = ::Vechile.new(registration_number, vechile_colour)
    end
  end

  def free
    self.vechile = nil
  end

  def free?
    self.vechile == nil
  end

  def registration_number
    vechile.number if vechile
  end

  def vechile_colour
    vechile.colour if vechile
  end
end
