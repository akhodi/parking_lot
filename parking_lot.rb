require_relative 'slot'

class ParkingLot
  attr_accessor :slots

  def initialize(number_of_slots)
    @slots = []
    number_of_slots.to_i.times do |i|
      number = i + 1
      slots[i] = Slot.new(number)
    end

    puts "Parking lot with #{number_of_slots} slots was successfully created"
  end

  def park(registration_number, vechile_colour)
    if next_free_slot
      puts "Allocated slot number : #{ next_free_slot.id } [registration_number: #{registration_number}]"
      next_free_slot.park(registration_number, vechile_colour)
    else
      parking_lot_full_handler(registration_number)
    end
  end

  def leave(slot_number)
    slot_number = slot_number.to_i
    if slot_number > 0 && slot_number <= slots.length
      slots[slot_number - 1].free
      puts "Slot number #{ slot_number.to_i } is free "
    else
      puts "Invalid slot number"
    end
  end

  def status
    puts "Slot No.\t Vechile Number\t Colour"
    slots.each do | slot |
      puts "#{ slot.id }\t\t #{ slot.registration_number }\t\t #{ slot.vechile_colour }" unless (slot.free?)
    end
  end

  def registration_numbers_for_cars_with_colour(colour)
    filtered_cars = filter_cars('registration_number', 'vechile_colour', colour)
    puts "Vechile number with colour #{colour} is: #{filtered_cars.compact.join(',')}"
  end

  def slot_numbers_for_cars_with_colour(colour)
    filtered_cars = filter_cars('id', 'vechile_colour', colour)
    puts "Slot number of the vechile_colour[#{colour}]: #{filtered_cars.compact.join(',')}"
  end

  def slot_number_for_registration_number(registration_number)
    slot = slots.find do |slot|
      slot.registration_number == registration_number
    end
    puts slot ? "Slot number of the registration_number[#{registration_number}]: #{slot.id}" : 'Not Found'
  end

  private
    def next_free_slot
      slots.find do |slot|
        slot.free?
      end
    end

    def parking_lot_full_handler(registration_number)
      puts "Sorry parking lot is full [registration_number: #{registration_number}]"
    end

    def filter_cars(filtered_value, filter_by, filter)
      slots.collect do |slot|
        slot.send(filtered_value) if slot.send(filter_by) == filter
      end
    end
end
