require_relative 'parking_lot'

if __FILE__ == $0
  # create parking lot
  parking_lot = ParkingLot.new(6)

  puts # new line

  # book parking slot
  parking_lot.park('A1234', 'White')
  parking_lot.park('A5678', 'White')
  parking_lot.park('A9012', 'Black')
  parking_lot.park('A3456', 'Red')
  parking_lot.park('A7890', 'Blue')
  parking_lot.park('B1234', 'Black')  

  puts # new line

  # leave
  parking_lot.leave(3)

  puts # new line

  # book parking slot
  parking_lot.park('B5678', 'White')
  parking_lot.park('B9012', 'Red')  

  puts # new line

  # get all data
  parking_lot.status

  puts # new line

  # get vechile numbers by vechile colour
  parking_lot.registration_numbers_for_cars_with_colour('White')

  puts # new line

  # get slot numbers by vechile colour
  parking_lot.slot_numbers_for_cars_with_colour('White')

  puts # new line

  # get slot number by vechile number
  parking_lot.slot_number_for_registration_number('A5678')
  parking_lot.slot_number_for_registration_number('A7890')
end
